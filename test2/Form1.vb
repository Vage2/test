﻿

Public Class Form1

    Dim _capture As clsOpenCVReader
    Dim play As Boolean
    Dim curframe As Double

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim ask As OpenFileDialog = New OpenFileDialog()
        ask.ShowDialog()
        _capture = New clsOpenCVReader(ask.FileName)

        curframe = 1
        play = False

        Label1.Text = _capture.FrameCount.ToString()
        Label2.Text = _capture.FPS.ToString()
        PictureBox1.Image = _capture.GetFrame(0)
        If _capture.FrameCount() > 0 Then
            NumericUpDown1.Maximum = _capture.FrameCount()
        Else
            NumericUpDown1.Minimum = 54000
        End If
        NumericUpDown1.Minimum = 0
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        _capture.Close()
        Me.Close()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        PictureBox1.Image = _capture.GetFrame(NumericUpDown1.Value)
        Label3.Text = _capture.TimeStamp.ToString()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click

        Do While curframe < _capture.FrameCount
            PictureBox1.Image = _capture.GetFrame(curframe)
            Label3.Text = curframe.ToString()
            curframe = curframe + NumericUpDown2.Value()

            System.Threading.Thread.Sleep(10)
            My.Application.DoEvents()
        Loop


    End Sub
End Class
