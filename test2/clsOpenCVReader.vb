﻿Imports Emgu.CV
Imports Emgu.CV.Util
Imports Emgu.CV.Structure

Public Class clsOpenCVReader

    Public _capture As VideoCapture

    Public Sub New(filepath As String)
        _capture = New VideoCapture(filepath)
    End Sub

    Public Sub Close()
        _capture.Dispose()
    End Sub

    Public ReadOnly Property FPS As Double
        Get
            FPS = _capture.GetCaptureProperty(CvEnum.CapProp.Fps)
        End Get
    End Property

    Public ReadOnly Property FrameCount As Double
        Get
            FrameCount = _capture.GetCaptureProperty(CvEnum.CapProp.FrameCount)
        End Get
    End Property

    Public ReadOnly Property Framesize As Size
        Get
            Framesize.Height = _capture.GetCaptureProperty(CvEnum.CapProp.FrameHeight)
            Framesize.Width = _capture.GetCaptureProperty(CvEnum.CapProp.FrameWidth)
        End Get
    End Property

    Public ReadOnly Property TimeStamp As Double
        Get
            TimeStamp = _capture.GetCaptureProperty(CvEnum.CapProp.PosMsec)
        End Get
    End Property

    Public Function GetFrame(FrameIndex As Integer) As Bitmap
        _capture.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames, FrameIndex)
        Return _capture.QueryFrame.Bitmap()
    End Function

End Class
